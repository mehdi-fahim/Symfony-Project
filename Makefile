build:
	docker-compose build
up:
	docker-compose up -d
stop:
	docker-compose stop
rm:
	docker-compose rm -f
kill:
	docker-compose kill

logs:
	docker-compose logs

tail:
	docker-compose logs -f 


destroy:
	make stop
	make kill
	make rm

reboot:
	make stop
	make kill
	make up
	docker-compose exec symfony_project php composer.phar update

install:
	make build
	make up
	docker-compose exec symfony_project php composer.phar update
	make stop
	make up

reinstall:
	make destroy
	make install

#		 	API				#
#===========================#
bash.web:
	docker-compose exec symfony_project bash

exec.web:
	docker-compose exec symfony_project

tail.web:
	docker-compose logs -f symfony_project

logs.web:
	docker-compose logs symfony_project
#===========================#
#		 	DB				#
#===========================#
bash.db:
	docker-compose exec database bash

exec.db:
	docker-compose exec database

tail.db:
	docker-compose logs -f database

logs.db:
	docker-compose logs database