<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnnonceType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre', TextType::class)
                ->add('description', TextareaType::class)
                ->add('prix', IntegerType::class)
                ->add('adresse', TextType::class)
                ->add('ville', TextType::class)
                ->add('code_postal', TextType::class)
                ->add('pays', TextType::class)
                ->add('nb_toilette', IntegerType::class)
                ->add('nb_salle_de_bain', IntegerType::class)
                ->add('nb_chambre', IntegerType::class)
                ->add('nb_personne', IntegerType::class)
                ->add('surface', IntegerType::class)
                ->add('date_ouverture', DateTimeType::class)
                ->add('date_fermeture', DateTimeType::class)
                ->add('image', FileType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Annonce'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_annonce';
    }
}
