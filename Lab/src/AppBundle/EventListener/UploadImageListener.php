<?php
namespace AppBundle\EventListener;

use AppBundle\Entity\Topic;
use AppBundle\Entity\Tutoriel;
use AppBundle\Entity\VentePAP;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use AppBundle\Entity\Article;
use AppBundle\Entity\Annonce;
use AppBundle\Service\ImageUpload;

class UploadImageListener
{
    private $uploader;

    public function __construct(ImageUpload $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        // upload only works for Product entities

        if ($entity == "Article"){
            $entity = new Article();
        } elseif ($entity == "Annonce"){
            $entity = new Annonce();
        } elseif ($entity == "Topic"){
            $entity = new Topic();
        } elseif ($entity == "Tutoriel"){
            $entity = new Tutoriel();
        } elseif ($entity == "VentePAP"){
            $entity = new VentePAP();
        }

        $file = $entity->getImage();

        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file);
        $entity->setImage($fileName);
    }
}