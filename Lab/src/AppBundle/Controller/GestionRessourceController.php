<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GestionRessourceController extends Controller
{
    /**
     * @Route("/gestion-ressources", name="gestion_ressources")
     */
    public function indexAction()
    {
        return $this->render('ressources/gestion-users.html.twig');
    }
}
