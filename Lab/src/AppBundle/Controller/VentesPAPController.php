<?php

namespace AppBundle\Controller;

use AppBundle\Entity\VentePAP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VentesPAPController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/gestion-ventes-pap", name="gestion_ventes_pap")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $ventesPaps = $em->getRepository('AppBundle:VentePAP')->findAll();
        return $this->render('ressources/gestion-ventes-pap.html.twig', array(
            'ventespap' => $ventesPaps,
        ));
    }

    /**
     * Finds and displays a ventesPap entity.
     *
     * @Route("/ventes-pap/{id}", name="ventes_pap_show")
     * @Method("GET")
     */
    public function showAction(VentePAP $ventesPap)
    {
        $deleteForm = $this->createDeleteForm($ventesPap);
        return $this->render('ventes-pap/show.html.twig', array(
            'ventespap' => $ventesPap,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ventesPap entity.
     *
     * @Route("/ventes-pap/{id}/edit", name="ventes_pap_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, VentePAP $ventesPap)
    {
        $deleteForm = $this->createDeleteForm($ventesPap);
        $editForm = $this->createForm('AppBundle\Form\VentePAPType', $ventesPap);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ventes_pap_show', array('id' => $ventesPap->getId()));
        }
        return $this->render('ventes-pap/edit.html.twig', array(
            'ventes-pap' => $ventesPap,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ventesPap entity.
     *
     * @Route("/ventes-pap/{id}", name="ventes_pap_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, VentePAP $ventesPap)
    {
        $form = $this->createDeleteForm($ventesPap);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ventesPap);
            $em->flush();
        }
        return $this->redirectToRoute('gestion_ventes_pap');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param VentePAP $ventesPap The ventesPap entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(VentePAP $ventesPap)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ventes_pap_delete', array('id' => $ventesPap->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
