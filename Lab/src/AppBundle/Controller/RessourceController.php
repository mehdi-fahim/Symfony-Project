<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annonce;
use AppBundle\Entity\Article;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Tutoriel;
use AppBundle\Entity\VentePAP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RessourceController extends Controller
{
    /**
     * @Route("/create-ressources", name="create_ressources")
     */
    public function createAction()
    {
        return $this->render('ressources/create-ressource.html.twig');
    }

    /**
     * @Route("/form-ressources/{ressource}", name="form_ressources")
     * @Method({"GET", "POST"})
     */
    public function formAction(Request $request, $ressource)
    {
        if ($ressource == "Article"){
            $ress = new Article();
        } elseif ($ressource == "Annonce"){
            $ress = new Annonce();
        } elseif ($ressource == "Tutoriel"){
            $ress = new Tutoriel();
        } elseif ($ressource == "Topic"){
            $ress = new Topic();
        } elseif ($ressource == "VentePAP"){
            $ress = new VentePAP();
        }

        $form = $this->createForm('AppBundle\Form\\'.$ressource.'Type', $ress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ress);
            $em->flush();

            return $this->redirectToRoute('validation_ressources');
        }
        return $this->render('ressources/form-ressource.html.twig', array(
            'ress_form' => $form->createView(),
        ));
    }

    /**
     * @Route("/validation-ressources", name="validation_ressources")
     */
    public function validationAction()
    {
        return $this->render('ressources/validation-ressource.html.twig');
    }

    /**
     * @Route("/list-ressource", name="list-ressources")
     */
    public function listRessourceAction()
    {
        return $this->render('ressources/list-ressource.html.twig');
    }
}
