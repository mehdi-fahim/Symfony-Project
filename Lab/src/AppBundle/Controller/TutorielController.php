<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tutoriel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TutorielController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/gestion-tutoriel", name="gestion_tutoriel")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $tutoriels = $em->getRepository('AppBundle:Tutoriel')->findAll();
        return $this->render('ressources/gestion-tutoriels.html.twig', array(
            'tutoriels' => $tutoriels,
        ));
    }

    /**
     * Finds and displays a tutoriel entity.
     *
     * @Route("/tutoriel/{id}", name="tutoriel_show")
     * @Method("GET")
     */
    public function showAction(Tutoriel $tutoriel)
    {
        $deleteForm = $this->createDeleteForm($tutoriel);
        return $this->render('tutoriel/show.html.twig', array(
            'tutoriel' => $tutoriel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tutoriel entity.
     *
     * @Route("/tutoriel/{id}/edit", name="tutoriel_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Tutoriel $tutoriel)
    {
        $deleteForm = $this->createDeleteForm($tutoriel);
        $editForm = $this->createForm('AppBundle\Form\TutorielType', $tutoriel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tutoriel_show', array('id' => $tutoriel->getId()));
        }
        return $this->render('tutoriel/edit.html.twig', array(
            'user' => $tutoriel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tutoriel entity.
     *
     * @Route("/tutoriel/{id}", name="tutoriel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Tutoriel $tutoriel)
    {
        $form = $this->createDeleteForm($tutoriel);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tutoriel);
            $em->flush();
        }
        return $this->redirectToRoute('gestion_tutoriel');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param Tutoriel $tutoriel The tutoriel entity
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Tutoriel $tutoriel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tutoriel_delete', array('id' => $tutoriel->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
